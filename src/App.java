import models.Circle;
import models.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        // Circle : Hình tròn
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        System.out.println("Circle 1:");
        System.out.println(circle1.getRadius());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println(circle1.toString());

        System.out.println("Circle 2:");
        System.out.println(circle2.getRadius());
        System.out.println(circle2.getRadius());
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());
        System.out.println(circle2.toString());

        // Rectangle: Hình chữ nhật

        Rectangle readable1 = new Rectangle();
        Rectangle readable2 = new Rectangle(3, 2);

        System.out.println("Readable 1:");
        System.out.println(readable1.getLength());
        System.out.println(readable1.getWidth());
        System.out.println(readable1.getArea1());
        System.out.println(readable1.getPerimeter());
        System.out.println(readable1.toString1());

       
         System.out.println("Readable 2:");
        System.out.println(readable2.getLength());
        System.out.println(readable2.getWidth());
        System.out.println(readable2.getArea1());
        System.out.println(readable2.getPerimeter());
        System.out.println(readable2.toString1());

    }
}
