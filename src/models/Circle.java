package models;

public class Circle {
    private double radius = 1.0;

    // khởi tạo có tham số
    public Circle(double radius) {
        this.radius = radius;
    }

    // khởi tạo KO tham số
    public Circle() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    // khởi tạo phương thức tính diện tích hình tròn
    public double getArea(){
        return Math.pow(this.radius, 2)* Math.PI;
    }

     // khởi tạo phương thức tính chi vi hình tròn
     public double getCircumference(){
        return 2 * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

    

    
}
