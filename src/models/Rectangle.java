package models;

public class Rectangle {
    float length = 1.0f;
    float width = 1.0f;
    
    // khởi tạo phương thức KO tham số
    public Rectangle() {
    }
    // khởi tạo phương thức có tham số
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
    public float getLength() {
        return length;
    }
    public void setLength(float length) {
        this.length = length;
    }
    public float getWidth() {
        return width;
    }
    public void setWidth(float width) {
        this.width = width;
    }
    // khởi tạo phương thức tính diện tích hình chữ nhật
    public double getArea1(){
        return this.length * this.width;
    }

     // khởi tạo phương thức tính chu vi hình chữ nhật
    public double getPerimeter(){
        return(this.length + this.width) * 2;
    }
    
    public String toString1() {
        return "Rectangle [length=" + length + ", width=" + width + "]";
    }

    

}
